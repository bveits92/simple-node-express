var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Boris - Express' });
});

/* GET about page. */
router.get('/about', function(req, res, next) {
  res.render('about', { title: 'About me' });
});

/* GET contact page */
router.get('/contact-me', function(req, res, next) {
  res.render('contact-me', { title: 'Contact me' });
});


module.exports = router;
